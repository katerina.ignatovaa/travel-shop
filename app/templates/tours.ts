import {Tour} from "../models/tours/tours";

export function getTourTemplate(tour:Tour, i:number):string{
    return `
        <div data-tour-index=${i} class="tour-block">
            <h2>${tour.name}</h2>
            <div>${tour.description}</div>
            <div>${tour.tourOperator}</div>   
            <div>${tour.price}</div>
        </div>
    `
}