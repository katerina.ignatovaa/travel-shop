import {getTours} from "@rest/tours";
import {openModal} from "@services/modal/modalService";
import {Tour} from "./models/tours/tours";
import {getTourTemplate} from "./templates/tours";

export let toursData:Tour[];

getTours().then((data:Tour[]):void => {
    toursData = data;
    renderToursData(toursData);
    console.log(toursData)
})

function renderToursData(toursData:Tour[]){
    const rootEl = document.querySelector('.main-app');
    const toursWrap = document.createElement('div');
    toursWrap.classList.add('tours-wrap');
    toursWrap.addEventListener(
        'click',
        (event) => {
            const targetEl = event.target as HTMLElement;
            const parentEl = targetEl.parentNode as HTMLElement;
            if(targetEl.hasAttribute('data-tour-index')){
                const tourIndex = targetEl.getAttribute('data-tour-index');
                openModal(Number(tourIndex))
            }
            else if(parentEl.hasAttribute('data-tour-index')){
                const tourIndex = parentEl.getAttribute('data-tour-index');
                openModal(Number(tourIndex))
            }
        })
    let tours: string = '';
    toursData.forEach(
        (tourData:Tour, i:number) => {
            const tour = getTourTemplate(tourData, i);
            tours += tour
        }
    )
    toursWrap.innerHTML = tours;
    rootEl.appendChild(toursWrap)
}