import {Modal} from "../../classes/modal";
import {Tour} from "../../models/tours/tours";
import {toursData} from "../../index";

export function openModal(i:number){
    const tours:Tour[] = toursData;
    const tourTemplate = `
        <h2>${tours[i].name}</h2>
        <div>${tours[i].description}</div>
        <div>${tours[i].tourOperator}</div>   
        <div>${tours[i].price}</div>
    `
    const modal = new Modal('');
    modal.openModal(tourTemplate)
}