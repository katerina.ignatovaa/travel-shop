export interface Tour{
    name:string,
    description:string,
    tourOperator:string,
    price:string
}