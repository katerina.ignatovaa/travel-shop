export class Modal  {
    private readonly id:string;
    public static modals:any[] = [];

    constructor(id) {
        const findEl = Modal.modals.find(modal => modal.id === this.id)
        if (findEl){
            Modal.removeById(this.id)
        }
        Modal.modals.push(this);
        this.id = id
    };

    private closeModal(event:MouseEvent):void {
        if((event.target as HTMLElement).classList.contains('close-modal')){
            this.removeModal()
        }
    };

    public openModal(template:string):void {
        const modalEl = document.createElement('div');
        modalEl.innerHTML = template;
        modalEl.classList.add('modal-element');
        modalEl.id = this.id;
        modalEl.addEventListener('click', this.closeModal)
        document.body.appendChild(modalEl)
    };

    public removeModal():void{
        const removeEl = document.getElementById(this.id);
        removeEl.removeEventListener('click', this.closeModal)
        removeEl.parentNode.removeChild(removeEl)
    };

    public static removeById(id: string):void {
        const findEl = Modal.modals.find(modal => modal.id === id);
        findEl.removeModal();
        Modal.modals.filter(modal => modal.id !== id)
    }
}